const gulp = require('gulp')
const del = require('del')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')

// Misc
function log (str, color) {
  console.log('\x1b[%dm%s\x1b[0m', color, str)
}
// Task functions
function compileSass () {
  return gulp.src('assets/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error',
      function (err) {
        log('[gulpfile.js:compileSass] failed to compile:\n' + err.toString(), 31)
        log('[gulpfile.js:compileSass] Waiting for changes...', 31)
        this.emit('end')
      }
    ))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./public/css/'))
}
function clean () {
  return del([
    'public/css/main.css'
  ])
}
function watcher (e) {
  log('[gulpfile.js:watcher] Currently watching ./assets/scss/*.scss\n', 36)
  return gulp.watch('./assets/scss/**/*.scss', function (cb) {
    process.stdout.write('\x1b[36m[gulpfile.js:watcher] Change detected --> compiling scss\x1b[0m')
    process.stdout.write(compileSass() ? '\x1b[32m --> compiled successfully\x1b[0m\n' : null)
    log('[gulpfile.js:watcher] Currently watching ./assets/scss/**/.scss\n', 36); cb()
  })
}
gulp.task('compile-sass', compileSass)
gulp.task('clean', clean)
gulp.task('watcher', watcher)
gulp.task('default', gulp.series(['clean', 'compile-sass', 'watcher']))
